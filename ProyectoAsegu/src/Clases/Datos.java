/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author steve
 */
public class Datos {
    public static Datos dat;
    private String Descripcion;
    private String Indice;
    private int kpi;

    private int meta;
    private String Tiempo;
    private String rp;

      public static Datos getInstance(){
        if (dat == null){
            dat = new Datos();
        }
        return dat;
    }
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getIndice() {
        return Indice;
    }

    public void setIndice(String Indice) {
        this.Indice = Indice;
    }

    public int getKpi() {
        return kpi;
    }

    public void setKpi(int kpi) {
        this.kpi = kpi;
    }
    public static Datos getDat() {
        return dat;
    }

    public static void setDat(Datos dat) {
        Datos.dat = dat;
    }

    public int getMeta() {
        return meta;
    }

    public void setMeta(int meta) {
        this.meta = meta;
    }

    public String getTiempo() {
        return Tiempo;
    }

    public void setTiempo(String Tiempo) {
        this.Tiempo = Tiempo;
    }

    public String getRp() {
        return rp;
    }

    public void setRp(String rp) {
        this.rp = rp;
    }
}
