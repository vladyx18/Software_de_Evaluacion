
package Clases;

/**
 *
 * @author steve
 */
public abstract class Variables  {
    private double x;
    private double u; 
    private double s;
    private double media;
    private double varianza;
    private double DesEstandar;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getU() {
        return u;
    }

    public void setU(double u) {
        this.u = u;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public double getVarianza() {
        return varianza;
    }

    public void setVarianza(double varianza) {
        this.varianza = varianza;
    }

    public double getDesEstandar() {
        return DesEstandar;
    }

    public void setDesEstandar(double DesEstandar) {
        this.DesEstandar = DesEstandar;
    }
    
}
