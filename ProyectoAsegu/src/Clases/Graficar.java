package Clases;

import Interfaz.Evaluar;
import Interfaz.*;
import java.text.DecimalFormat;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


/**
 *
 * @author steve
 */
public class Graficar {
    
   public void Graficar(String dato1, String dato2, String dato3){
 Procedimientos correr= new Procedimientos();    
      BuscarDato dato = new BuscarDato();
      correr.setX(Double.parseDouble(dato1));
      correr.setU(Double.parseDouble(dato2));
      correr.setS(Double.parseDouble(dato3));
      DecimalFormat df = new DecimalFormat("#0.00");
                            
                            double respuesta = dato.buscar(Double.parseDouble(df.format(correr.calcular())));
            
      ChartPanel panel;
        JFreeChart chart = null;
            XYSplineRenderer renderer = new XYSplineRenderer();
            XYSeriesCollection dataset =  new XYSeriesCollection();
            ValueAxis x = new NumberAxis();
            ValueAxis y = new NumberAxis();
            XYSeries serie = new XYSeries("Datos");
            XYPlot plot;
            Evaluar.lineas.removeAll();                   
                      serie.add(-1,0);
                      serie.add(0,respuesta);
                      serie.add(1,0);
         
                dataset.addSeries(serie);
                x.setLabel("Eje X");
                y.setLabel("Eje Y");
                plot = new XYPlot(dataset, x, y, renderer);
                chart = new JFreeChart(plot);
                chart.setTitle("Gráfica de campana");

    panel = new ChartPanel(chart);
        panel.setBounds(5,10, 410, 350);
        
   
            Evaluar. lineas.add(panel);
            Evaluar.lineas.repaint();
            Evaluar.txtresultado.setText(df.format(correr.calcular()));
            Evaluar.txttotal.setText(String.valueOf(respuesta));
            Evaluar.porcentaje.setText(String.valueOf(df.format(respuesta*100)));
  
   }
   public void graficar2(String dato1, String dato2, String dato3){

 Procedimientos correr= new Procedimientos();    
      BuscarDato dato = new BuscarDato();
      correr.setX(Double.parseDouble(dato1));
      correr.setU(Double.parseDouble(dato2));
      correr.setS(Double.parseDouble(dato3));
      DecimalFormat df = new DecimalFormat("#0.00");
                            
                            double respuesta = dato.buscar(Double.parseDouble(df.format(correr.calcular())));
            
      ChartPanel panel;
        JFreeChart chart = null;
            XYSplineRenderer renderer = new XYSplineRenderer();
            XYSeriesCollection dataset =  new XYSeriesCollection();
            ValueAxis x = new NumberAxis();
            ValueAxis y = new NumberAxis();
            XYSeries serie = new XYSeries("Datos");
            XYPlot plot;
          RegistrosIndices.lineasdos.removeAll();                   
                      serie.add(-1,0);
                      serie.add(0,respuesta);
                      serie.add(1,0);
         
                dataset.addSeries(serie);
                x.setLabel("Eje X");
                y.setLabel("Eje Y");
                plot = new XYPlot(dataset, x, y, renderer);
                chart = new JFreeChart(plot);
                chart.setTitle("Gráfica de campana");

    panel = new ChartPanel(chart);
        panel.setBounds(5,10, 410, 350);
        

       RegistrosIndices. lineasdos.add(panel);
          RegistrosIndices.lineasdos.repaint();
           RegistrosIndices.txtresultado2.setText(df.format(correr.calcular()));
          RegistrosIndices.txttotal2.setText(String.valueOf(respuesta));
           RegistrosIndices.porcentaje2.setText(String.valueOf(df.format(respuesta*100)));
   }
}
