/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author steve
 */
public class InsertarKPI {

 static Connection cn= null;
 static Statement s=null;
 static ResultSet rs=null;
 PreparedStatement pst = null;
   double datos;  
    public void Insertar(String nombre,String descripcion,int meta,String tiempo,String rp){
      
        
        
     try {
                //Para conectarnos a nuestra base de datos
                String url = "jdbc:oracle:thin:@localhost:1521:XE";
                // Establecemos los valores de cadena de conexión, usuario y contraseña
                cn = DriverManager.getConnection(url, "probabilidad","123");
                //Para ejecutar la consulta
                s = cn.createStatement();
                //Ejecutamos la consulta y los datos lo almacenamos en un ResultSet
             
                pst = cn.prepareStatement("INSERT INTO kpis (NOMBREKPI,DESCRIPCIONKPI,meta,tiempom,rp) VALUES(?,?,?,?,?)");
                pst.setString(1,nombre);
                pst.setString(2,descripcion);
                pst.setInt(3,meta);
                pst.setString(4,tiempo);
                pst.setString(5,rp);
            
                pst.executeQuery();
           
                System.out.println("Los valores han sido agregados a la base de datos ");
            } catch (SQLException ex) {
                Logger.getLogger(InsertarKPI.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
                               }

           
}
