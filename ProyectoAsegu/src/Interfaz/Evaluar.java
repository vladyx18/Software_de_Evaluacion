package Interfaz;


import Clases.Graficar;
import static java.awt.image.ImageObserver.HEIGHT;
import javax.swing.JOptionPane;
import Clases.Datos;



/**
 *
 * @author steve
 */
public class Evaluar extends javax.swing.JInternalFrame {

    /**
     * Creates new form Evaluar
     */
    public Evaluar() {
        initComponents();
        this.setSize(995, 550);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        txtdos = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtuno = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtres = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        lineas = new javax.swing.JPanel();
        txtresultado = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        porcentaje = new javax.swing.JLabel();
        a1 = new javax.swing.JLabel();
        a2 = new javax.swing.JLabel();
        a3 = new javax.swing.JLabel();

        setClosable(true);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel1.setBackground(new java.awt.Color(63, 122, 156));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Ingreso de Datos a Evaluar  :");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, -1));

        txttotal.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txttotal.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(txttotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 70, 60, 30));
        jPanel1.add(txtdos, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 90, 70, 30));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("a");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, 10, 30));
        jPanel1.add(txtuno, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 90, 70, 30));

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Intervalos:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, 120, 30));
        jPanel1.add(txtres, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 140, 140, 30));

        jButton1.setText("Evaluar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 140, 90, 30));

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel9.setText("Grafica  de Resultados ");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 30, -1, -1));

        lineas.setBackground(new java.awt.Color(63, 122, 156));
        lineas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout lineasLayout = new javax.swing.GroupLayout(lineas);
        lineas.setLayout(lineasLayout);
        lineasLayout.setHorizontalGroup(
            lineasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        lineasLayout.setVerticalGroup(
            lineasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(lineas, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 110, 500, 380));

        txtresultado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtresultado.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(txtresultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 70, 120, 30));

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("varianza de Intervalos:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 170, 30));

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Z:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, 40, 30));

        jLabel12.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("P(z):");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 70, 40, 30));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("=");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 80, -1, -1));

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("%");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 80, 20, -1));

        porcentaje.setBackground(new java.awt.Color(255, 255, 255));
        porcentaje.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(porcentaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 70, 40, 30));

        a1.setBackground(new java.awt.Color(204, 0, 0));
        a1.setForeground(new java.awt.Color(255, 255, 255));
        a1.setText("*");
        jPanel1.add(a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 20, 20));

        a2.setBackground(new java.awt.Color(204, 0, 0));
        a2.setForeground(new java.awt.Color(255, 255, 255));
        a2.setText("*");
        jPanel1.add(a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 70, 20, 20));

        a3.setBackground(new java.awt.Color(204, 0, 0));
        a3.setForeground(new java.awt.Color(255, 255, 255));
        a3.setText("*");
        jPanel1.add(a3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 120, 20, 20));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1029, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

      if (txtuno.getText().equals("")|| txtdos.getText().equals("")|| txtres.getText().equals("")){  
                JOptionPane.showMessageDialog(null, "Debe de llenar todo los campos con * color Rojo ","Error", HEIGHT);
                  
                if(txtuno.getText().equals("")){a1.setForeground(java.awt.Color.RED);}else{a1.setForeground(java.awt.Color.WHITE);}
                if(txtdos.getText().equals("")) {a2.setForeground(java.awt.Color.RED);}else{a2.setForeground(java.awt.Color.WHITE);}
                if(txtres.getText().equals("")) {a3.setForeground(java.awt.Color.RED);}else{a3.setForeground(java.awt.Color.WHITE);}
      }else{
            Graficar abrir = new Graficar();
      abrir.Graficar(txtuno.getText(),txtdos.getText(),txtres.getText());}
        
                         // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel a1;
    private javax.swing.JLabel a2;
    private javax.swing.JLabel a3;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JPanel lineas;
    public static javax.swing.JLabel porcentaje;
    public static javax.swing.JTextField txtdos;
    public static javax.swing.JTextField txtres;
    public static javax.swing.JLabel txtresultado;
    public static javax.swing.JLabel txttotal;
    public static javax.swing.JTextField txtuno;
    // End of variables declaration//GEN-END:variables
}
