/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import java.text.DecimalFormat;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import proyectoasegu.BuscarDato;
import proyectoasegu.Datos;
import proyectoasegu.Procedimientos;


/**
 *
 * @author steve
 */
public class Evaluar extends javax.swing.JInternalFrame {

    /**
     * Creates new form Evaluar
     */
    public Evaluar() {
        initComponents();
        this.setSize(995, 473);
     Datos rg = Datos.getInstance();
        txtindice.setText(rg.getIndice());
        txtkpi.setText(String.valueOf(rg.getKpi()));
        txtdescripcion.setText(rg.getDescripcion());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtdescripcion = new javax.swing.JLabel();
        txtindice = new javax.swing.JLabel();
        txtkpi = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtdos = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtuno = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtres = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        lineas = new javax.swing.JPanel();
        txtresultado = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setClosable(true);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel1.setBackground(new java.awt.Color(63, 122, 156));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Ingreso de Datos a Evaluar  :");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, -1, -1));

        txttotal.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txttotal.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(txttotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 60, 60, 30));

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nombre de Indice :");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 180, 30));

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Numero KPI:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, 100, 30));

        txtdescripcion.setForeground(new java.awt.Color(255, 255, 255));
        txtdescripcion.setText("datos");
        jPanel1.add(txtdescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 150, 220, 30));

        txtindice.setForeground(new java.awt.Color(255, 255, 255));
        txtindice.setText("datos");
        jPanel1.add(txtindice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, 230, 30));

        txtkpi.setForeground(new java.awt.Color(255, 255, 255));
        txtkpi.setText("datos");
        jPanel1.add(txtkpi, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 110, 210, 30));

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Datos :");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Descripcion:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 110, 30));
        jPanel1.add(txtdos, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 240, 70, 30));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("a");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 240, 10, 30));
        jPanel1.add(txtuno, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, 70, 30));

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Intervalos:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 120, 30));
        jPanel1.add(txtres, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 290, 140, 30));

        jButton1.setText("Evaluar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 290, 90, 30));

        jLabel9.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel9.setText("Grafica  de Resultados ");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 20, -1, -1));

        javax.swing.GroupLayout lineasLayout = new javax.swing.GroupLayout(lineas);
        lineas.setLayout(lineasLayout);
        lineasLayout.setHorizontalGroup(
            lineasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        lineasLayout.setVerticalGroup(
            lineasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(lineas, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 100, 500, 380));

        txtresultado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        txtresultado.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(txtresultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 60, 120, 30));

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("varianza de Intervalos:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 170, 30));

        jLabel10.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Z:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 60, 40, 30));

        jLabel12.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("P(z):");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 60, 40, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    Procedimientos correr= new Procedimientos();    
      BuscarDato dato = new BuscarDato();
      correr.setX(Double.parseDouble(txtuno.getText()));
      correr.setU(Double.parseDouble(txtdos.getText()));
      correr.setS(Double.parseDouble(txtres.getText()));
      DecimalFormat df = new DecimalFormat("#0.00");
                            double respuesta = dato.buscar(Double.parseDouble(df.format(correr.calcular())));
            
        ChartPanel panel;
        JFreeChart chart = null;
            XYSplineRenderer renderer = new XYSplineRenderer();
            XYSeriesCollection dataset =  new XYSeriesCollection();
            ValueAxis x = new NumberAxis();
            ValueAxis y = new NumberAxis();
            XYSeries serie = new XYSeries("Datos");
            XYPlot plot;
            lineas.removeAll();                   
                      serie.add(-1,0);
                      serie.add(0,respuesta);
                      serie.add(1,0);
         
                dataset.addSeries(serie);
                x.setLabel("Eje X");
                y.setLabel("Eje Y");
                plot = new XYPlot(dataset, x, y, renderer);
                chart = new JFreeChart(plot);
                chart.setTitle("Gráfica de campana");

        panel = new ChartPanel(chart);
        panel.setBounds(5,10, 410, 350);
        
    
            lineas.add(panel);
            lineas.repaint();
            txtresultado.setText(df.format(correr.calcular()));
            txttotal.setText(String.valueOf(respuesta));
 
                         // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel lineas;
    private javax.swing.JLabel txtdescripcion;
    private javax.swing.JTextField txtdos;
    private javax.swing.JLabel txtindice;
    private javax.swing.JLabel txtkpi;
    private javax.swing.JTextField txtres;
    private javax.swing.JLabel txtresultado;
    private javax.swing.JLabel txttotal;
    private javax.swing.JTextField txtuno;
    // End of variables declaration//GEN-END:variables
}
