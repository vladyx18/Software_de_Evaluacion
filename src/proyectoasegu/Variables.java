
package proyectoasegu;

/**
 *
 * @author steve
 */
public abstract class Variables  {
    private double x;
    private double u; 
    private double s;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getU() {
        return u;
    }

    public void setU(double u) {
        this.u = u;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }
    
}
