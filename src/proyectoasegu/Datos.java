/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoasegu;

/**
 *
 * @author steve
 */
public class Datos {
    public static Datos dat;
    private String Descripcion;
    private String Indice;
    private int kpi;

      public static Datos getInstance(){
        if (dat == null){
            dat = new Datos();
        }
        return dat;
    }
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getIndice() {
        return Indice;
    }

    public void setIndice(String Indice) {
        this.Indice = Indice;
    }

    public int getKpi() {
        return kpi;
    }

    public void setKpi(int kpi) {
        this.kpi = kpi;
    }
}
